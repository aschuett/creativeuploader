<?php // no direct access
defined('_JEXEC') or die('Restricted access');

$document = &JFactory::getDocument();
$user =& JFactory::getUser();
$params = &JComponentHelper::getParams( 'com_creativeuploader' );
$params->merge( new JParameter( $row->params ) );

$document->addScript( 'components/com_creativeuploader/includes/jquery.tools.min.js' );
$document->addScript( 'components/com_creativeuploader/includes/jquery.dataTables.min.js' );
$document->addScript( 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js' );
$document->addStyleSheet( 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/ui-darkness/jquery-ui.css' );
$document->addStyleSheet( 'components/com_creativeuploader/includes/table.css' );

$conf =& JFactory::getConfig();
$sitename = $conf->getValue('config.sitename');

?>

<div style="position: relative; overflow: hidden;">
<div style="float: left; position: relative; left: 50%;">
<div style="float: left; position: relative; left: -50%; min-width: 675px;">
	<h1 style="text-align: center;">File Manager</h1> 
	<p style="text-align: center;">Here you can manage the files uploaded to <?php echo $sitename; ?>.</p>
	<table id="filelist" class="display" style="min-width: 675px">
		<thead>
			<tr>
				<th>
					ID
				</th>
				<th>
					<input type="checkbox" name="toggle" value="" id="checkall" />
				</th>
				<th>
					Manage
				</th>
				<th>
					Title
				</th>
				<th>
					Department
				</th>
				<th>
					Uploaded By
				</th>
			</tr>
		</thead>
		<?php
		$k = 0;
		for ($i=0, $n=count( $this->data ); $i < $n; $i++)	{
			$row = &$this->data[$i];
			$checked 	= JHTML::_('grid.id',   $i, $row->id );
			//$link 		= JRoute::_( 'index.php?option=com_creativeuploader&controller=creativeuploader&task=edit&cid[]='. $row->id );
			?>
			<tr class="<?php echo "row$k"; ?>">
				<td>
					<?php echo $row->id; ?>
				</td>
				<td>
					<?php echo $checked; ?>
				</td>
				<td>
					<!-- This is where control buttons will go, i.e. delete -->
					<a href="/upload/files/<?php echo $row->filename; ?>" style="margin-right: 6px"><img src="/components/com_creativeuploader/includes/save.png"></a><img src="/components/com_creativeuploader/includes/trash.png">
				</td>
				<td>
					<a href="/upload/files/<?php echo $row->filename; ?>"><?php echo $row->title; ?></a>
				</td>
				<td>
					<?php $department = explode('|',$row->department); echo '<a href="mailto:' . $department[1] . '">' . $department[0] . '</a>'; ?>
				</td>
				<td>
					<a href="mailto:<?php echo $row->email; ?>"><?php echo $row->name; ?></a>
				</td>
			</tr>
			<?php
			$k = 1 - $k;
		}
		?>
	</table>
</div>
</div>
</div>

<script>
jQuery(document).ready(function() {
	jQuery('#filelist').dataTable({
		"bJQueryUI": true,
		"aoColumnDefs": [
			{"bSortable": false, "bVisible":false, "aTargets": [1]},
			{"bSortable": false, "aTargets": [2]}
		]
	});
} );

jQuery(function () { // this line makes sure this code runs on page load
	jQuery('#checkall').click(function () {
		jQuery(this).parents('table').find(':checkbox').attr('checked', this.checked);
	});
});
</script>