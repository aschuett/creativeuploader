<?php // no direct access
defined('_JEXEC') or die('Restricted access');

?>
<h1>Post dump</h1>

<p>Shows the form items posted.</p>

<table>
	<tr>
		<th>Name</th>
		<th>Value</th>
	</tr>
	<?php $count = 0; foreach ($_POST as $name => $value) { ?>
	<tr class="<?php echo $count % 2 == 0 ? 'alt' : ''; ?>">
		<td><?php echo $name ?></td>
		<td><?php echo nl2br(htmlentities(stripslashes($value))) ?></td>
	</tr>
	<?php } ?>
</table>

<?php

JUtility::sendMail($_POST['userEmail'], $_POST['name'], 'a.khondee@gmail.com', 'File Upload from creat.com', 'File Upload from creat.com to department ' . $_POST['department'] . ".\r\n Title:\r\n" . $_POST['title'] . "\r\nDescription:\r\n" . $_POST['description'] . "\r\n", 1, '', '', '', $_POST['userEmail'], $_POST['name']);

?>