<?php // no direct access
defined('_JEXEC') or die('Restricted access');

$document = &JFactory::getDocument();
$user =& JFactory::getUser();
$params = &JComponentHelper::getParams( 'com_creativeuploader' );
$params->merge( new JParameter( $row->params ) );


$document->addScript( 'components/com_creativeuploader/includes/jquery.tools.min.js' );
$document->addScript( 'components/com_creativeuploader/includes/silverlight.js' );
$document->addScript( 'components/com_creativeuploader/includes/plupload.min.js' );
$document->addScript( 'components/com_creativeuploader/includes/plupload.silverlight.min.js' );
$document->addScript( 'components/com_creativeuploader/includes/jquery.plupload.queue.min.js' );
$document->addScript( 'components/com_creativeuploader/includes/functions.js' );
$document->addScript( 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js' );
$document->addStyleSheet( 'components/com_creativeuploader/includes/style.css' );
$document->addStyleSheet( 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/base/jquery-ui.css' );
$conf =& JFactory::getConfig();
$sitename = $conf->getValue('config.sitename');











$uploaderJS = <<<EOD
// Custom example logic
//jQuery.noConflict();
jQuery(function() {
	var response;
	var inputs = jQuery('#uploadform :input').validator();

	jQuery('#uploadBtn').button({ text: 'Start Upload', icons: {primary:'ui-icon-carat-1-e'} }).hide().click(function()
		{
			if (!inputs.data('validator').checkValidity())
				return false;
			jQuery('#uploadBtn').hide();
			response = jQuery.parseJSON(jQuery.ajax({ type: 'POST', url: 'index.php?option=com_creativeuploader&task=store', async: false, data: 'uploaded=0&title=' + jQuery('input[name=title]').val() + '&description=' + jQuery(':input[name=description]').val() + '&name=' + jQuery(':input[name=name]').val() + '&email=' + jQuery(':input[name=userEmail]').val() + '&department=' + jQuery('input[name=radio]:checked', '#uploadform').val() }).responseText);
			// sync: false means JS will pause until complete
			// up.refresh(); // Reposition Flash/Silverlight
			jQuery('#cancelupload').show().click(function() { window.location.reload(); });;
			jQuery('#formStep1 :input').attr('disabled', true);
			setTimeout(function() { uploader.start(); }, 50);
			setTimeout(function() { jQuery('#formStep1').fadeTo('slow',0.5); }, 5); // fade the first part of the form to 50% - still need to disable form elements
			jQuery('#progressBar').show();
		});
	jQuery('#pickfiles').button({ text: 'Select File', icons: {primary:'ui-icon-plusthick'} });
	jQuery('#cancelupload').button({ text: 'Cancel Upload', icons: {primary:'ui-icon-closethick'} }).hide();
	jQuery('#department').buttonset();
	if (Silverlight.isInstalled('4.0')) {
		var uploader = new plupload.Uploader({
			runtimes : 'silverlight',
			browse_button : 'pickfiles',	// ID of file pick button
			unique_names : true,			// Assign each uploaded file a unique name
			chunk_size : '{$params->get('chunk_size')}',			// Chunks larger than 512KB seem to make things slower!
			multi_selection: false,			// Allow only one file to be selected at a time
			multipart : true,
			max_file_size : '{$params->get('max_upload')}',
			url : '/components/com_creativeuploader/includes/upload.php',
			silverlight_xap_url : '/components/com_creativeuploader/includes/plupload.silverlight.xap'//,
			//filters : [
			//	{title : 'Image files', extensions : 'jpg,gif,png'},
			//	{title : 'Zip files', extensions : 'zip'}
			//]
		});
	
		uploader.bind('Init', function(up, params) {
			jQuery('#pickfiles').show();
			uploader.refresh();
		});
	
		uploader.bind('FilesAdded', function(up, files) {
			jQuery('#pickfiles').hide();
			jQuery('#uploadBtn').show();
			jQuery('#filelist').html('Currently selected file:');
			jQuery.each(files, function(i, file) {
				jQuery('#filelist').append(
					'<div id=\'\' + file.id + \'\'>' +
					file.name + ' (' + plupload.formatSize(file.size) + ')' +
				'</div>');
			});
		});
		
		uploader.bind('UploadFile', function(up, file) {
			// Called when the file starts to upload
		});
		
		uploader.bind('FileUploaded', function(up, file, res) {
			// file.target_name = filename on the server
			// file.name = original filename
			// file.size = size in bytes	
			setTimeout(function() {
				jQuery('.smallish-progress-text').html('100% complete');
				jQuery('.smallish-progress-bar').width('100%');
				jQuery('#cancelupload').hide();
				var response2 = jQuery.parseJSON(res.response);
				if (typeof response2.error != 'undefined') {
					alert('Error ' + response.error.code + ': ' + response.error.message);
					window.location.reload();
				} else {
					// There was no problem uploading
					// After file gets uploaded successfully:
					console.log(response);
					var sendData = 'id=' + response.id + '&uploaded=1&filename=' + file.target_name + '&filename_original=' + file.name;
					var response3 = jQuery.parseJSON(jQuery.ajax({ type: 'POST', url: 'index.php?option=com_creativeuploader&task=store', async: false, data: sendData }).responseText);
					alert('File uploaded successfully.');
					window.location.reload();
				}
			}, 50);
		});
	
		uploader.bind('UploadProgress', function(up, file) {
			// file.loaded = number of bytes uploaded
			// file.name = the file's name
			// file.percent = percentage uploaded
			// file.size = total file size in bytes
			// file.status = QUEUED, UPLOADING, FAILED, or DONE
			var status;
			if (file.status == 2) { status = 'Uploading'; } else { status = ''; }
			jQuery('.smallish-progress-text').html(status + ' ' + file.percent + '% complete (' + convertSizeToReadable(file.loaded) + ' @ ' + convertSizeToReadable(up.total.bytesPerSec) + '/sec)');
			jQuery('.smallish-progress-bar').width(file.percent + '%');
		});
		
		uploader.bind('Error', function(up,error) {
			console.log(error);
			alert('Error ' + error.code + ': ' + error.message);
			//window.location.reload();
		});
		uploader.init();
		//uploader.refresh();
	} else {
		// Silverlight 4 or greater isn't installed
		jQuery('#formStep1').css('text-align','center');
		jQuery('#formStep1').html('<a href=\'#\' onClick=\'javascript:Silverlight.getSilverlight(\'\');\'><img style=\'border-style: none;\' src=\'/includes/silverlight/getsilverlight.png\'></a>');
		//Silverlight.getSilverlight('');
	}
	
});
EOD;
$document->addScriptDeclaration( $uploaderJS );


?>
<!-- <h1><?php echo $this->uploader; ?></h1> -->

<div style="position: relative; overflow: hidden;">
<div style="float: left; position: relative; left: 50%;">
<div style="float: left; position: relative; left: -50%;">
<div id="uploadform">
	<h1 style="text-align: center;">File Uploader</h1> 
	<p style="text-align: center;">You may use this form to upload files directly to <?php echo $sitename; ?>.  PC or Mac with Silverlight required.	Maximum upload is <?php echo $params->get( 'max_upload' ); ?>.</p>
	<fieldset id="formStep1"> 
		<?php 
		$departments = explode("\n", $params->get( 'departments' ));
		//print_r($departments);
		if (!is_array($departments)) {
			// No departments exist, use default email
			echo "\t\t" . '<div id="department" class="radioList"><input type="radio" id="radio1" name="radio" value="' . $params->get( 'default_email' ) . '" style="display: none;" /></div>';
		} elseif (count($departments) == 1) {
			// Only one department, hide it
			echo "\t\t" . '<div id="department" class="radioList"><input type="radio" id="radio1" name="radio" value="' . $departments[0] . '" style="display: none;" /></div>';
		} else {
			// Show all departments
		?><p>
			<label>Department:</label><br />
			<div id="department" class="radioList"><?php
			foreach ($departments as $value) {
				$department = explode('|',$value);
				echo "\t\t\t\t" . '<input type="radio" id="radio' . $department[0] . '" name="radio" value="' . $value . '" /><label for="radio' . $department[0] . '">' . $department[0] . '</label>' . "\r\n";
			} ?>
			</div>
		</p><?php
		} ?>
		<p>
			<label>Title:</label><br />
			<input type="text" size="68" name="title" required="required" maxlength="50" data-message="Please enter a title for your file upload" />
		</p>
		<p>
			<label>Description:</label><br />
			<textarea rows="6" cols="50" name="description" required="required" data-message="Please enter a description for your file upload"></textarea>
		</p>
		<!--
		<b>Additional information captured by this form:</b><br />
		Name: <?php echo $user->name; ?><br />
		Email: <?php echo $user->email; ?><br />
		-->
		
		<input type="hidden" name="name" id="name" value="<?php echo $user->name; ?>" />
		<input type="hidden" name="userEmail" id="userEmail" value="<?php echo $user->email; ?>" />
		
		<input type="hidden" name="option" value="com_creativeuploader" />
		<input type="hidden" name="view" value="submitfile" />
		<input type="hidden" name="filename" value="" />
		<input type="hidden" name="filename_original" value="" />
	</fieldset>
	<fieldset id="formStep2" style="">
		<div class="upload" style="text-align: center;">
			<div id="filelist" style="margin: 8px;"></div>
			<!-- <input type="button" id="pickfiles" value="Pick File" style="display: none;" /> -->
			<button id="pickfiles">Pick File</button>
			<button id="uploadBtn">Upload file...</button>
			<button id="cancelupload">Cancel Upload</button>
		</div>
		<div class="smallish-progress-wrapper" id="progressBar" style="display: none;">
			<div class="smallish-progress-bar" style="width: 0px;"></div>
			<div class="smallish-progress-text">0%</div>
		</div>		
	</fieldset>
</div>
</div>
</div>
</div>