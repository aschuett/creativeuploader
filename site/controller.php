<?php
/**
 * Hello World default controller
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */

jimport('joomla.application.component.controller');

/**
 * Hello World Component Controller
 *
 * @package		HelloWorld
 */
class CreativeUploaderController extends JController
{
	/**
	 * Method to display the view
	 *
	 * @access	public
	 */
	function display()
	{
		parent::display();
	}
	
	public function check() {
		// Access this on a page with the following code:
		//	function loggedIn() {
		//		if(1 == $.ajax({ type: "GET", url: "index.php?option=com_user&task=check", async: false }).responseText) {
		//			return true;
		//		}
		// 		return false;
		//	}
	
		$app = JFactory::getApplication();
		$user =& JFactory::getUser();
		echo ($user->guest) ? "0" : "1";
		//don't send the full page, just the ajax response above
		$app->close();
	}
	
	function someTask()
    {
		JFactory::getDocument()->setMimeEncoding("application/json");
		
		// Get the application object.
		$app = JFactory::getApplication();

		// Get the model.
		$model = $this->getModel('files'); // class CreativeUploaderModelCreativeUploader extends JModel

		// Get the data from the model.
		$data = $model->getData();
		// Check for errors.
		//if ($model->getError()) {
			// Do something.
		//}

		// Echo the data as JSON.
		echo json_encode($data);		
		$app->close();
    }
	
	function store()
    {
		JFactory::getDocument()->setMimeEncoding("application/json");
		
		// Get the application object.
		$app = JFactory::getApplication();

		// Get the model.
		$model = $this->getModel('File'); // class CreativeUploaderModelCreativeUploader extends JModel

		// Get the data from the model.
		if ($model->store())
		{
			echo ('{"jsonrpc" : "2.0", "result" : null, "id" : ' . $model->getId() . '}');
			if ($_POST['uploaded'] == TRUE) {
				$model = $this->getModel('file');
				$model->setId($_POST['id']);
				$data = $model->getData();
				
				$department = explode('|',$data->department);
				// JUtility::sendMail($from, $fromname, $recipient, $subject, $body, $mode=0, $cc=null, $bcc=null, $attachment=null, $replyto=null, $replytoname=null)
				JUtility::sendMail($data->email, $data->name, $department[1], 'File Upload from creat.com', 'A file has just been uploaded for ' . $department[0] . ".\r\nTitle: " . $data->title . "\r\nDescription: " . $data->description . "\r\nDownload it here: " . JURI::root() . "/upload/files/" . $data->filename, 0, '', '', '', 'todd@creat.com', 'Todd Schuett');
			}
		} else {
			echo ('{"jsonrpc" : "2.0", "error" : {"code": 200, "message": "Failed to store file data in database."}, "id" : "id"}');
		}	
		$app->close();
    }

}
?>
