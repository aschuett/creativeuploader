
function convertSizeToReadable(bytes) {
	//Don't go larger than MB
	if (bytes > (10 * 1024 * 1024)) {
		// > 10 MB
		return (bytes/1024/1024).toFixed(0) + " MB"; // Return MB
	} else {
		return (bytes/1024).toFixed(0) + " KB"; // Return KB
	}
}