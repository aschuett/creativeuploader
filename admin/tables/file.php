<?php
/**
 * Hello World table class
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://docs.joomla.org/Developing_a_Model-View-Controller_Component_-_Part_4
 * @license		GNU/GPL
 */

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * Hello Table class
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class TableFile extends JTable
{
	/**
	 * Primary Key
	 *
	 * @var int
	 */
	var $id = null;

	/**
	 * @var string
	 */
	var $filename = null;
	
	/**
	 * @var string
	 */
	var $filename_original = null;
	
	/**
	 * @var string
	 */
	var $uploaded = null;
	
	/**
	 * @var string
	 */
	var $title = null;
	
	/**
	 * @var string
	 */
	var $description = null;
	
	/**
	 * @var string
	 */
	var $department = null;
	
	/**
	 * @var string
	 */
	var $name = null;
	
	/**
	 * @var string
	 */
	var $email = null;

	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 */
	function TableFile(& $db) {
		parent::__construct('#__creativeuploader_files', 'id', $db);
	}
}