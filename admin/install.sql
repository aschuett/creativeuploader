DROP TABLE IF EXISTS `#__hello`;

CREATE TABLE `#__hello` (
  `id` int(11) NOT NULL auto_increment,
  `greeting` varchar(25) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

INSERT INTO `#__hello` (`greeting`) VALUES ('Hello, World!'), ('Bonjour, Monde!'), ('Ciao, Mondo!');


DROP TABLE IF EXISTS `#__creativeuploader_files`;
CREATE TABLE `#__creativeuploader_files` (
  `id` int(11) NOT NULL auto_increment,
  `filename` varchar(255),
  `filename_original` varchar(255),
  `uploaded` BOOLEAN NOT NULL DEFAULT FALSE,
  `title` varchar(255),
  `description` varchar(255),
  `department` varchar(255),
  `name` varchar(255),
  `email` varchar(255),
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
