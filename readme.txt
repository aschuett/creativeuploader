Creative Uploader
Internal Release - 1.33 12/18/2011


Installation
------------
Please install through the Joomla component installer.

Upgrading
---------
There is currently no upgrade path from previous versions.  A complete uninstall must be completed prior to installing the latest version.

Outstanding Issues
------------------
see Google Docs list

Known Limitations
-----------------
-Creative Uploader has not been tested on versions of Joomla other than 1.5.25
-Upload sizes are restricted by locally available web storage
-Downloads are not password protected

Change Log
----------
Release 1.33 - 12/17/2011

-Changed "title" field on upload form to be required instead of having specific character type requirements
-Fixed error message for "description" firled on upload form
-Fixed department display for one or no departments in list, no longer displays "Department" header
-
